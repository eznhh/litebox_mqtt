#include "spiffs.h"

String readFile(fs::FS &fs, const char* path) {
  Serial.printf("[SPIFFS / readFile()] Reading file: %s\r\n", path);
  File file = fs.open(path, "r");
  if (!file || file.isDirectory()) {
    Serial.println("[SPIFFS / readFile()] Empty file or failed to open file");
    return String();
  }
  Serial.println("[SPIFFS / readFile()] Read from file:");
  String fileContent;
  while (file.available()) {
    fileContent += String((char)file.read());
  }
  Serial.println(fileContent);
  return fileContent;
}

void writeFile(fs::FS &fs, const char* path, const char* message) {
  Serial.printf("[SPIFFS / writeFile()] Writing file: %s\r\n", path);
  File file = fs.open(path, "w");
  if (!file) {
    Serial.println("[SPIFFS / writeFile()] Failed to open file for writing");
    return;
  }
  if (file.print(message)) {
    Serial.println("[SPIFFS / writeFile()] File written");
  } else {
    Serial.println("[SPIFFS / writeFile()] Write failed");
  }
}

/*
void writeFile_float(fs::FS &fs, const char* path, float var) {
  Serial.printf("[SPIFFS / writeFile_float()] Writing file: %s\r\n", path);
  File file = fs.open(path, "w");
  if (!file) {
    Serial.println("[SPIFFS / writeFile_float()] Failed to open file for writing");
    return;
  }
  if (file.printf("%.2f", var)) {
    Serial.println("[SPIFFS / writeFile_float()] File written");
  } else {
    Serial.println("[SPIFFS / writeFile_float()] Write failed");
  }
}
*/

String processor(const String& var) {                 // Replaces placeholder with stored values
  Serial.println("[SPIFFS / processor()] "+var);

  if (var == "ssid") {
    return readFile(SPIFFS, "/ssid.txt");
  }
  else if (var == "gsmid") {
    return readFile(SPIFFS, "/gsmid.txt");
  }
  else if (var == "apikey") {
    return readFile(SPIFFS, "/apikey.txt");
  }
  else if (var == "url") {
    return readFile(SPIFFS, "/url.txt");
  }
  else if (var == "temperature") {
    return readFile(SPIFFS, "/temperatures.txt");    // TODO: Werte auslesen bis ";" und Wert aus Datei entfernen (für Aufruf in nächster Zeile in Datei (siehe webServer.cpp)
  }
  else if (var == "messintervall") {
    return readFile(SPIFFS, "/messintervall.txt");
  }
  return String();
}

void spiffsBegin() {    // TODO (?)
  if (!SPIFFS.begin()) {
    Serial.println("[spiffsBegin()] An Error has occured while mounting SPIFFS");
    Serial.println("");
    return;
  }
}

void spiffsReconnect() {
  if (readFile(SPIFFS, "/ssid.txt") != "" && readFile(SPIFFS, "/password.txt") != "") {                   // Verbindet mit neuem Netzwerk/URL nach erfolgreichem Ablesen der Werte
    //ssid = readFile(SPIFFS, "/ssid.txt");           //neue var für ssid und pw erstellen und
    //password = readFile(SPIFFS, "password.txt");    //nach SPIFFS Auslesung mit neuem Netzwerk verbinden
    Serial.println("[spiffsReconnect()] Mit neuem Netzwerk verbinden (Neue SSID/PW)");
  } else {
    writeFile(SPIFFS, "/ssid.txt", ssid);
    Serial.println("[spiffsReconnect()] SSID/PW ohne Inhalt. SSID wird zurückgesetzt");

    // TODO: Funktion schreiben für Neuverbindung mit Netzwerk/URL nach Wertänderung und evtl. Fehlerausgabe

  }
  if (readFile(SPIFFS, "/gsmid.txt") != "" && readFile(SPIFFS, "/apikey.txt") != "") {
    //gsmid = readFile(SPIFFS, "/gsmid.txt");
    //apikey = readFile(SPIFFS, "/apikey.txt");
    Serial.println("[spiffsReconnect()] Datenlogger-URL wird gewechselt (Neue GSMID/API Key)");
  } else {
    writeFile(SPIFFS, "/gsmid.txt", gsmid);
    writeFile(SPIFFS, "/apikey.txt", apikey);
    Serial.println("[spiffsReconnect()] Datenlogger-Wert ist leer. Datenlogger wird zurückgesetzt");
  }
  Serial.println("");
}

void spiffsWriteTemp(std::vector<sensor> ds18b20) {          // TODO: dynamische Wertübergabe
  uint8_t x = 0;
  String payload;
  Serial.println("[spiffsWriteTable()] Writing temperatures into SPIFFS");
  for (uint8_t j = 0; j < 4; j++) {
    Serial.println("[spiffsWriteTable()] TEMPERATUREN WERDEN EINGELESEN VON PIN D" + String(j + 1) + "\n");
    uint8_t x_old = x;
    for (; x < sensorCount[j] + x_old; x++) {               // Alternativ: ; "for(uint8_t x = 0; x < sensorCount[j]; x++){...}" und [x + x_old] (statt [x]) in Elementaufruf
      payload += String(ds18b20.at(x).temperature) + ";";
    }
  }
  const char* message = payload.c_str();
  writeFile(SPIFFS, "/temperatures", message);
  Serial.println("");
}
