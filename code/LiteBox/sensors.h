#ifndef SRC_SENSORS_H_
#define SRC_SENSORS_H_

//#define ARX_VECTOR_DEFAULT_SIZE 256

#include <Arduino.h>
#include <ArxContainer.h>
#include <OneWire.h>

static OneWire oneWire[] = { 5, 4, 0, 2 };

const uint8_t oneWireCount = sizeof(oneWire)/sizeof(OneWire); // TODO: misleading name, rename to "oneWireSize"

struct sensor {
  uint8_t number;
  String sensorName;
  String pin;
  byte address[8];
  float temperature;
  bool crcErr;
};
static std::vector<sensor> ds18b20;
// From https://stackoverflow.com/questions/4303513/push-back-vs-emplace-back :
//static std::vector<std::pair<byte [8], float> > ds18b20;

// Time interval for measuring the temperatures
static uint16_t messintervall = 30; // uint16_t < 66000 (fast 18 Stunden); in Sekunden; TODO: in finaler Version 60 (* 1000)

// 
static uint8_t oneWireArr[] = { 5, 4, 0, 2 }; // TODO: automatische Übernahme der Werte von oneWire[] zu bus[]. Temporärer Array oneWireArr[]...
static uint8_t sensorCount[4];                // TODO: "addressSpace" ?
static uint8_t j = 0;
static bool isDone = false;                   // TODO: move to setup()(?)


void sensorsInit(uint8_t j, bool& isDone, std::vector<sensor>& ds18b20, uint8_t sensorCount[]);
void sensorsRequestTemp(std::vector<sensor>& ds18b20, uint8_t sensorCount[]);
//TODO: void readSensor() mit Algorithmus aus den 2 for-Schleifen mit geeigneten Parametern (möglich?)

#endif
