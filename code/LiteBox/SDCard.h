#ifndef SRC_SDCARD_H_
#define SRC_SDCARD_H_

#include <SPI.h>
#include <SD.h>
#include <NTPClient.h>
#include <WiFiUDP.h>
#include "sensors.h"
#include "webServer.h"
#include "MQTT.h"

const uint8_t chipSelect = 15;
const long utcOffsetInSeconds = 3600;
static String months[] = {"Januray", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
static String weekDays[] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

static WiFiUDP ntpUDP;
static NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", utcOffsetInSeconds);

static unsigned long epochTime;       //NTP
static String formattedTime;
static int currentHour;
static int currentMinute;
static int currentSecond;
static String weekDay;
static struct tm *ptm;  //Get a time structure
static int monthDay;
static int currentMonth;
static String currentMonthName;
static int currentYear;
static String currentDate;  //Print complete date:
static String currentTime;

void SDbegin();
void SDwriteTemp(std::vector<sensor> ds18b20, NTPClient timeClient, uint8_t sensorCount[]);
void NTPBegin(NTPClient &timeClient);
void NTPUpdate(NTPClient &timeClient);

#endif
