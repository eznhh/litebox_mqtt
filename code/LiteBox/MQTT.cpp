#include "MQTT.h"

void MQTTbegin(EspMQTTClient &client) {
  // Optionnal functionnalities of EspMQTTClient :
  client.enableDebuggingMessages(); // Enable debugging messages sent to serial output
  client.enableHTTPWebUpdater(); // Enable the web updater. User and password default to values of MQTTUsername and MQTTPassword. These can be overrited with enableHTTPWebUpdater("user", "password").
  client.enableLastWillMessage("public/ingest/10bcfe50aeux", "I am going offline");  // You can activate the retain flag by setting the third parameter to true
}

// This function is called once everything is connected (Wifi and MQTT)
// WARNING : YOU MUST IMPLEMENT IT IF YOU USE EspMQTTClient
//void onConnectionEstablished() {
//  // Subscribe to "mytopic/test" and display received message to Serial
//  client.subscribe("public/ingest/10bcfe50aeux", [](const String & payload) {
//    Serial.println("============================================SUBSCRIPTION============================================");
//    Serial.println(payload);
//    Serial.println("====================================================================================================");
//  });
///*
//  // Subscribe to "mytopic/wildcardtest/#" and display received message to Serial
//  client.subscribe("public/ingest/10bcfe50aeux", [](const String & topic, const String & payload) {
//    Serial.println("(From Offline) topic: " + topic + ", payload: " + payload);
//  });
//  
//    // Publish a message to "mytopic/test"
//    client.publish("mytopic/test", "This is a message"); // You can activate the retain flag by setting the third parameter to true
//
//    // Execute delayed instructions
//    client.executeDelayed(5 * 1000, []() {
//      client.publish("mytopic/wildcardtest/test123", "This is a message sent 5 seconds later");
//    });
//  */
//}

void sendTemptoServer(EspMQTTClient &client, std::vector<sensor> ds18b20, NTPClient timeClient, uint8_t sensorCount[]) {
  String message = "[";
  uint8_t j = 0;
  for (uint8_t i = 0; i < 4; i++) {
    Serial.println("[sendTemptoServer()] Payload wird erstellt (PIN D" + String(i + 1) + ")");
    uint8_t j_old = j;
    for (; j < sensorCount[i] + j_old; j++) {               // Alternativ: ; "for(uint8_t j = 0; j < sensorCount[i]; j++){...}" und [j + j_old] (statt [j]) in Elementaufruf
      message += "{\"ArrivalTime\": \"" + String(currentDate) + " " + String(timeClient.getFormattedTime()) + "\",\"Tag\": \"TMP\",\"TP\": \"" + String(j + 1) + "\",\"Val\": \"" + String(ds18b20.at(j).temperature) + "\"}";
      if ((i + 1) == 4 && (j + 1) == (sensorCount[i] + j_old)) message += "]";
      else message += ", ";
    }
  }
  Serial.println("[sendTemptoServer()] Payload wird gesendet...");
  if(!client.publish("public/ingest/10bcfe50aeux", message)) SDwriteTemp(ds18b20, timeClient, sensorCount);
  Serial.println("");
}

void sendSDtoServer(EspMQTTClient &client, unsigned long lastMillis) {
  File root = SD.open("/");
  root.rewindDirectory();

  Serial.print("[secureClient / sendSDtoServer()] lastMillis = ");    // DEBUG
  Serial.println(lastMillis);
  Serial.print("[secureClient / sendSDtoServer()] millis() = ");
  Serial.println(millis());
  while ((lastMillis + (messintervall * 1000)) > millis()) {
    File file = root.openNextFile();

    String fileName = file.name();
    Serial.print("[secureClient / sendSDtoServer()] Aktueller Pfad/Dateiname: ");     // DEBUG
    Serial.println(fileName);
    //if(!SD.exists(fileName)) break;
    if (!file) break;
    String message = "";
    while (file.available())
      message += (char)file.read();
    //file.close();

    Serial.println("[secureClient / sendSDtoServer()] Ausgelesener Payload (message) vorm Senden an Server:");  //DEBUG
    Serial.print(message);

    if (client.publish("public/ingest/10bcfe50aeux", message)) {
      if (SD.remove(fileName)) {                // Datei wird gelöscht
        Serial.print("[secureClient / sendSDtoServer()] \"");
        Serial.print(fileName);
        Serial.println("\" wurde erfolgreich gelöscht!");
      } else {
        Serial.print("[secureClient / sendSDtoServer()] \"");
        Serial.print(fileName);
        Serial.println("\" konnte nicht gelöscht werden!");
      }
    }
    else Serial.println("[secureClient / sendSDtoServer()] Unstimmige Payload oder Verbindung unterbrochen. Wurde nicht gesendet. Datei \"" + fileName + "\" nicht gelöscht!");
    delay(500);
  }
}
void MQTTloop(EspMQTTClient &client) {
  client.loop();
}
