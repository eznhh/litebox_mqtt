#ifndef SRC_SPIFFS_H_
#define SRC_SPIFFS_H_

//#include <Hash.h>
#include <FS.h>
#include "sensors.h"

#define WLAN_WORK static const char* ssid = "EZN-Network"; static const char* password = "03709591784203449569";
#define WLAN_MOBILE static const char* ssid = "K"; static const char* password = "12345678";
#define WLAN_HOME static const char* ssid = "WILHELM.TEL-DGDFH47YDJ"; static const char* password = "47468112177600718796";

WLAN_WORK

static const char* gsmid = "400708";
static const char* apikey = "2A391DCB0871DEC7001AF54832721B39";
static String url = "https://el.ez-nord.com/api/v1/datapoints/" + String(gsmid) + "?APIKEY=" + String(apikey);

static uint8_t procTemp = 0;

static const char* PARAM_SSID = "ssid";
static const char* PARAM_PASSWORD = "password";
static const char* PARAM_GSMID = "gsmid";
static const char* PARAM_APIKEY = "apikey";
static const char* PARAM_MESSINTERVALL = "messintervall";

extern String readFile(fs::FS &fs, const char* path);
extern void writeFile(fs::FS &fs, const char* path, const char* message);
//extern void writeFile_float(fs::FS &fs, const char* path, float var);
extern String processor(const String& var);
extern void spiffsBegin();
extern void spiffsReconnect();
extern void spiffsWriteTemp(std::vector<sensor> ds18b20);

#endif
