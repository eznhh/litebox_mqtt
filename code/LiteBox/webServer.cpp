#include "webServer.h"

//void notFound(AsyncWebServerRequest *request) {
//  request->send(404, "text/plain", "Not found");
//}

//void connectWiFi(){
//  for (uint8_t t = 3; t > 0; t--) {
//    Serial.printf("[connectWiFi()] WAIT %d...\n", t);
//    Serial.flush();
//    delay(1000);
//  }
//  WiFi.mode(WIFI_STA);
//  
//  Serial.print("Verbinden mit ");
//  Serial.println(ssid);
//  WiFi.begin(ssid, password);
//  while(WiFi.status() != WL_CONNECTED){
//    delay(500);
//    Serial.print(".");
//  }
//
//
///*---------------------------------------------Debug---------------------------------------------*/
//  Serial.println("SSID: ");
//  Serial.println(WiFi.SSID());
//  Serial.println("psk: ");
//  Serial.println(WiFi.psk());
//  //Serial.println("BSSID: ");
//  //Serial.println(String(WiFi.BSSID()));
//  Serial.println("BSSIDstr: ");
//  Serial.println(WiFi.BSSIDstr());
//  Serial.println("RSSI: ");
//  Serial.println(WiFi.RSSI());
//  Serial.println("MAC Adresse: ");
//  Serial.println(WiFi.macAddress());
//  Serial.println("Subnetz Maske: ");
//  Serial.println(WiFi.subnetMask());
//  Serial.println("Gateway IP: ");
//  Serial.println(WiFi.gatewayIP());
//  Serial.println("DNS IP: ");
//  Serial.println(WiFi.dnsIP());
//  Serial.println("Hostname: ");
//  Serial.println(WiFi.hostname());
///*-----------------------------------------------------------------------------------------------*/
//
//  
//  Serial.println("");
//  Serial.println("Mit WiFi verbunden.");
//  Serial.println("IP Adresse: ");
//  Serial.println(WiFi.localIP());
////  server.begin();
//}

//void handleWebServer(std::vector<sensor> ds18b20, const char* index1, const char* index2, uint8_t sensorCount[]){
//  server.on("/", HTTP_GET, [ds18b20, index1, index2, sensorCount](AsyncWebServerRequest *request){       // Send web page with input fields to client
////    const char* index = index_html(ds18b20, index1, index2, sensorCount);
//    request->send_P(200, "text/html", index_html(ds18b20, index1, index2, sensorCount), processor);
//  });
//  server.on("/get", HTTP_GET, [](AsyncWebServerRequest *request){     // Send a GET request to <ESP_IP>/get?PARAM_INPUT=<inputMessage>
//    String inputMessage;
//    if(request->hasParam(PARAM_SSID) && request->hasParam(PARAM_PASSWORD)){     // Speichert abgesendete SSID/PW in zugehöriger Textdatei; TODO: Passwortdatei verschlüsseln?
//      inputMessage = request->getParam(PARAM_SSID)->value();
//      writeFile(SPIFFS, "/ssid.txt", inputMessage.c_str());
//      inputMessage = request->getParam(PARAM_PASSWORD)->value();
//      writeFile(SPIFFS, "/password.txt", inputMessage.c_str());
//    }
//    else if(request->hasParam(PARAM_GSMID) && request->hasParam(PARAM_APIKEY)){ // GSMID & APIKEY; TODO: APIKEY-Datei verschlüsseln?
//      inputMessage = request->getParam(PARAM_GSMID)->value();
//      writeFile(SPIFFS, "/gsmid.txt", inputMessage.c_str());
//      inputMessage = request->getParam(PARAM_APIKEY)->value();
//      writeFile(SPIFFS, "/apikey.txt", inputMessage.c_str());
//    }
//    else if(request->hasParam(PARAM_MESSINTERVALL)){                            // Messintervall
//      inputMessage = request->getParam(PARAM_MESSINTERVALL)->value();
//      writeFile(SPIFFS, "/messintervall.txt", inputMessage.c_str());
//    }
//    else{
//      inputMessage = "[SPIFFS) Error: No message sent";
//    }
//    Serial.println(inputMessage);
//    request->send(200, "text/text", inputMessage);
//  });
//  server.on("/restart", HTTP_GET, [](AsyncWebServerRequest *request){
//    request->send_P(200, "text/html", restart_html);
//  });
//  server.on("/restarting", HTTP_GET, [](AsyncWebServerRequest *request){
//    Serial.println("\n\n\n==============================");
//    Serial.println("\tLITEBOX WIRD RESTARTET");
//    Serial.println("==============================\n\n\n");
//    ESP.restart();
//  });
//  server.onNotFound(notFound);
//  server.begin();
//}

//const char* index_html(std::vector<sensor> ds18b20, const char* index1, const char* index2, uint8_t sensorCount[]) {
//  String table = "";
//  String top_html = index1;
//  String bottom_html = index2;
//  uint8_t x = 0;
//  Serial.println("[index_html()] Writing HTML");
//  for (uint8_t j = 0; j < 4; j++) {
//    Serial.println("[index_html()] TABELLENDATEN WERDEN EINGELESEN VON PIN D" + String(j + 1) + "\n");
//    uint8_t x_old = x;
//    for (; x < sensorCount[j] + x_old; x++) {               // Alternativ: ; "for(uint8_t x = 0; x < sensorCount[j]; x++){...}" und [x + x_old] (statt [x]) in Elementaufruf
//      String addrStr, crc;
//      for (int8_t i = 8; i > 0; i--) {                     // TODO: i >= 0
//        if (ds18b20.at(x).address[i - 1] < 16) addrStr += "0";
//        addrStr += String(ds18b20.at(x).address[i - 1], HEX);
//      }
//      if (ds18b20.at(x).crcErr) crc = "CRC Fehler!";
//      else crc = "Online";
//
//      addrStr.toUpperCase();
//      uint8_t z = ds18b20.at(x).number + 1;
//      
//      table += "<tr><td>" + String(z) + "</td><td>" + ds18b20.at(x).sensorName + "</td><td>" + ds18b20.at(x).pin + "</td><td>" + addrStr + "</td><td>"+String(ds18b20.at(x).temperature)+"&deg;C</td><td>" + crc + "</td></tr>";
//    }
//  }
//  Serial.println("[index_html()] index1 = \n" + String(index1));
//  Serial.println("[index_html()] table = \n" + table);
//  Serial.println("[index_html()] index2 = \n" + String(index2));
//  String html = index1 + table + index2;
//  const char* index = html.c_str();
//  return index;
//}
