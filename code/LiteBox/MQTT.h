#ifndef SRC_MQTT_H_
#define SRC_MQTT_H_

#include "EspMQTTClient.h"
//#include "webServer.h"
#include "SDCard.h"
#include "spiffs.h"

//const char* clientName = IDvonMessibox; // ID der Messibox automatisch einlesen und als Client Name für Server angeben

//static EspMQTTClient client(
//  ssid,                               //SSID
//  password,                           //PW
//  "ingest.hive-test.de.eneriq.com",   // MQTT Broker server ip
//  "bbd0bd86",                         // Can be omitted if not needed
//  "59be2f02ju8?hk1.",                 // Can be omitted if not needed
//  "400123",                           // Client name that uniquely identify your device
//  1883                                // The MQTT port, default to 1883. this line can be omitted
//);

static unsigned long lastMillis;

void MQTTbegin(EspMQTTClient &client);
//void onConnectionEstablished();
void sendTemptoServer(EspMQTTClient &client, std::vector<sensor> ds18b20, NTPClient timeClient, uint8_t sensorCount[]);
void sendSDtoServer(EspMQTTClient &client, unsigned long lastMillis);
void MQTTloop(EspMQTTClient &client);

#endif
