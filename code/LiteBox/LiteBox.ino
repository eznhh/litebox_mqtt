/*
   *Die Litebox sendet die Messwerte der Sensoren über eine verschlüsselte Verbindung an die Datenbank der Energiezentrale-Nord.
   *Bei fehlender Zusendung (kein Internet, Datenbank nicht erreichbar, etc.) werden die Werte auf der SD Karte gespeichert.
   *Bei erfolgreichem Wiederaufbau zum Server, werden die, mit einem Timestamp versehenen, Daten auf der SD Karte
   *stetig "parallel" zur eigentlichen Messung und Upload der aktuellen Messwerte geuploadet.
   *Die Litebox stellt außerdem einen Webserver zur Konfiguration zur Verfügung.

   Written by: Khalid Farooqi
   Written for: Energiezentrale Nord
*/
#include <Arduino.h>
#include "OTA.h"
#include "MQTT.h"

EspMQTTClient client(
  ssid,                               //SSID
  password,                           //PW
  "ingest.hive-test.de.eneriq.com",   // MQTT Broker server ip
  "bbd0bd86",                         // Can be omitted if not needed
  "59be2f02ju8?hk1.",                 // Can be omitted if not needed
  "400123",                           // Client name that uniquely identify your device
  1883                                // The MQTT port, default to 1883. this line can be omitted
);


void setup() {

  delay(1000);
  Serial.begin(115200);
  delay(10);
  Serial.flush();
  Serial.println("\r\n");

  NTPBegin(timeClient);

  for (j = 0; j < 4; j++) {
    Serial.println("");
    Serial.println("DATEN WERDEN ANGEFORDERT VON PIN D" + String(j + 1));
    Serial.println("");
    isDone = false;
    while (!isDone) sensorsInit(j, isDone, ds18b20, sensorCount);
  }
  Serial.println("+++++++++++++++++++++++++++++++++++++++++++++++ ENDE DER PINS [INIT] +++++++++++++++++++++++++++++++++++++++++++++++\n");
  sensorsRequestTemp(ds18b20, sensorCount);

  spiffsBegin(); delay(100); // Nachricht nur bei Fehler

//  connectWiFi();

//  OTAbegin();
  MQTTbegin(client); delay(1000);

//  handleWebServer(ds18b20, index1, index2, sensorCount);

  SDbegin();
}






void onConnectionEstablished() {
  // Subscribe to "mytopic/test" and display received message to Serial
//  client.subscribe("public/ingest/10bcfe50aeux", [](const String & payload) {
//    Serial.println("============================================SUBSCRIPTION============================================");
//    Serial.println(payload);
//    Serial.println("====================================================================================================");
//  });
/*
  // Subscribe to "mytopic/wildcardtest/#" and display received message to Serial
  client.subscribe("public/ingest/10bcfe50aeux/offline", [](const String & topic, const String & payload) {
    Serial.println("(From Offline) topic: " + topic + ", payload: " + payload);
  });
  
    // Publish a message to "mytopic/test"
    client.publish("mytopic/test", "This is a message"); // You can activate the retain flag by setting the third parameter to true

    // Execute delayed instructions
    client.executeDelayed(5 * 1000, []() {
      client.publish("mytopic/wildcardtest/test123", "This is a message sent 5 seconds later");
    });
  */
}








/*
   1. NTPUpdate(): Zeitvariablen werden bestimmt
   2. sensorsRequestTemp(): Messwerte werden abgefragt und in Variablen gespeichert
   3. sendTemptoServer(): Messwerte werden an den Server gesendet
   4. spiffsReconnect(): Werte wie SSID&PW und GSMID&APIKey werden aus dem SPIFFS ausgelesen. ESP verbindet sich ggf. mit neuem (ausgelesenen) Netzwerk/Datenlogger
   5. spiffsWriteTemp(): Messwerte werden im SPIFFS gespeichert
   6. sendSDtoServer(): Sendet in verbliebener Zeit des Intervalls die Dateien mit abgespeicherten Messwerten vereinzelt an den Server.
      Ist der Server nicht erreichbar (bspw. keine Internetverbindung), werden Messwerte in SD Karte gespeichert. Bei erfolgreicher Sendung werden Daten gelöscht
      SDwriteTemp(): Messwerte werden in einer zeitsignierten Datei auf der SD Karte abgespeichert
*/
void loop() {
  onConnectionEstablished();

  lastMillis = millis();

//  OTAhandle(); delay(1000);

  MQTTloop(client); delay(1000);

  NTPUpdate(timeClient); delay(1000);

  sensorsRequestTemp(ds18b20, sensorCount); delay(1000);
  sendTemptoServer(client, ds18b20, timeClient, sensorCount); delay(1000);

//  spiffsReconnect(); delay(1000);
//  spiffsWriteTemp(ds18b20); delay(1000);

  //SDwriteTemp(ds18b20, timeClient, sensorCount); delay(1000); //verschoben nach sendTempToServer(); hier als DEBUG
  sendSDtoServer(client, lastMillis); delay(1000);

  Serial.println("");
  Serial.println("[LOOP()] Nächste Temperaturmessung...");

  Serial.print("[LOOP()] Eine Messung in 60 Sekunden. Restzeit läuft aus");
  while ((lastMillis + (messintervall * 1000)) > millis()) {
    Serial.print(".");
    delay(1000);
  }
  Serial.println("");
  Serial.println("================================================================ NEXT LOOP ================================================================");
  Serial.println("\r\n");
}
