#include "sensors.h"

void sensorsInit(uint8_t j, bool& isDone, std::vector<sensor>& ds18b20, uint8_t sensorCount[]) {
  byte i;
  byte addr[8];
  String addrStr = "";

  delay(100);

  if ( !oneWire[j].search(addr)) {
    Serial.println("No more addresses.\n");
    oneWire[j].reset_search();
    isDone = true;
    delay(250);
    return;
  }

  Serial.print("ROM = ");
  for ( i = 8; i > 0; i--) {        // TODO: i >= 0
    if (addr[i - 1] < 16) {
      Serial.print("0");
      addrStr += "0";
    }
    Serial.print(addr[i - 1], HEX);
    addrStr += String(addr[i - 1], HEX);
  }

  //ds18b20.push_back({addr, 0});
  ds18b20.emplace_back();
  Serial.print("[sensorsRequestData()] ds18b20.back().address[] = "); 
  for (int i = 0; i < 8; i++) {
    ds18b20.back().address[i] = addr[i];
    Serial.print(String(ds18b20.back().address[i], HEX));
  } Serial.println("");
  Serial.print("[sensorsRequestData()] addr[] (raw address string) = ");
  for (int i = 0; i < 8; i++) {
    Serial.print(String(addr[i], HEX));
  } Serial.println("");
  Serial.println("[sensorsRequestData()] Sensoradresse (addrStr) = " + addrStr);

  if (OneWire::crc8(addr, 7) != addr[7]) {
    Serial.println("CRC is not valid!");
    ds18b20.back().crcErr = true;
    return;
  } else ds18b20.back().crcErr = false;

  // the first ROM byte indicates which chip
  switch (addr[0]) {
    case 0x10:
      Serial.println("  Chip = DS18S20");  // or old DS1820
      //type_s = 1;
      ds18b20.back().sensorName = "DS18S20";
      break;
    case 0x28:
      Serial.println("  Chip = DS18B20");
      //type_s = 0;
      ds18b20.back().sensorName = "DS18B20";
      break;
    case 0x22:
      Serial.println("  Chip = DS1822");
      //type_s = 0;
      ds18b20.back().sensorName = "DS1822";
      break;
    default:
      Serial.println("Device is not a DS18x20 family device.");
      ds18b20.back().sensorName = "Non-DS18x20";
      return;
  }

  oneWire[j].reset();
  oneWire[j].select(addr);
  oneWire[j].write(0x44, 1);        // start conversion, with parasite power on at the end

  sensorCount[j]++;
  Serial.println("[sensorsRequestData()] sensorCount[" + String(j) + "] = " + String(sensorCount[j]) + "\n");

  delay(1000);     // maybe 750ms is enough, maybe not
  // we might do a oneWire.depower() here, but the reset will take care of it.

}

void sensorsRequestTemp(std::vector<sensor>& ds18b20, uint8_t sensorCount[]) {
  uint8_t x = 0;
  for (uint8_t j = 0; j < 4; j++) {
    Serial.println("MESSWERTE WERDEN ANGEFORDERT VON PIN D" + String(j + 1) + "\n");
    uint8_t x_old = x;
    for (; x < sensorCount[j] + x_old; x++) {               // Alternativ: ; "for(uint8_t x = 0; x < sensorCount[j]; x++){...}" und [x + x_old] (statt [x]) in Elementaufruf
      byte present = 0;
      byte type_s;
      byte data[12];
      byte addr[8];                                         // TODO: if line 101 (this line + 7) works, leave this one out
      float celsius, fahrenheit;

      ds18b20.at(x).number = x;
      ds18b20.at(x).pin = String(oneWireArr[j]) + " (D" + String(j+1) + ")";
      for(uint8_t i=0; i < 8; i++) addr[i] = ds18b20.at(x).address[i];      // TODO: if line 101 (this line + 4) works, leave this one out

      present = oneWire[j].reset();

      oneWire[j].select(ds18b20.at(x).address);   //oneWire[j].select(addr);      
      oneWire[j].write(0xBE);                     // Read Scratchpad

      Serial.print("  Data = ");
      Serial.print(present, HEX);
      Serial.print(" ");
      for (uint8_t i = 0; i < 9; i++) {           // we need 9 bytes
        data[i] = oneWire[j].read();
        Serial.print(data[i], HEX);
        Serial.print(" ");
      }
      Serial.print(" CRC=");
      Serial.print(OneWire::crc8(data, 8), HEX);
      Serial.println();

      // Convert the data to actual temperature
      // because the result is a 16 bit signed integer, it should
      // be stored to an "int16_t" type, which is always 16 bits
      // even when compiled on a 32 bit processor.
      int16_t raw = (data[1] << 8) | data[0];

      if(ds18b20.at(x).address[0] == 0x10) type_s = 1;
      else type_s = 0;
      
      if (type_s) {
        raw = raw << 3; // 9 bit resolution default
        if (data[7] == 0x10) {
          // "count remain" gives full 12 bit resolution
          raw = (raw & 0xFFF0) + 12 - data[6];
        }
      } else {
        byte cfg = (data[4] & 0x60);
        // at lower res, the low bits are undefined, so let's zero them
        if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
        else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
        else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
        //// default is 12 bit resolution, 750 ms conversion time
      }
      celsius = (float)raw / 16.0;

      fahrenheit = celsius * 1.8 + 32.0;
      Serial.print("  Temperature = ");
      Serial.print(celsius);
      Serial.print(" Celsius, ");
      Serial.print(fahrenheit);
      Serial.println(" Fahrenheit");

      // --------------- Added -----------------------------------------------------

      ds18b20.at(x).temperature = celsius;
      Serial.print("[sensorsRequestData()] ds18b20.at(" + String(x) + ").address (address) = ");
      for(uint8_t i = 0; i < 8; i++) Serial.print(String(ds18b20.at(x).address[i], HEX));
      Serial.println("");
      Serial.println("[sensorsRequestData()] ds18b20.at(" + String(x) + ").temperature (temperatures) = " + String(ds18b20.at(x).temperature) + "\n");
      delay(1000);
    }
  }
  Serial.println("+++++++++++++++++++++++++++++++++++++++++++++++ ENDE DER PINS +++++++++++++++++++++++++++++++++++++++++++++++\n");
}
