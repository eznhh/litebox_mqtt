#include "SDCard.h"

void SDbegin() {
  Serial.print("[SDbegin] Initializing SD card...");

  if (!SD.begin(chipSelect)) {
    Serial.println(" initialization failed. Things to check:");
    Serial.println("[SDbegin] 1. is a card inserted?");
    Serial.println("[SDbegin] 2. is your wiring correct?");
    Serial.println("[SDbegin] 3. did you change the chipSelect pin to match your shield or module?");
    Serial.println("[SDbegin] Note: press reset or reopen this serial monitor after fixing your issue!");
    while (true);
  }

  Serial.println("[SDbegin] initialization done.");
  Serial.println("");
}

void SDwriteTemp(std::vector<sensor> ds18b20, NTPClient timeClient, uint8_t sensorCount[]) {
  Serial.println("");
  
  String message = "[";
  uint8_t j = 0;
  for (uint8_t i = 0; i < 4; i++) {
    Serial.println("[SDwriteTemp()] Payload wird erstellt (PIN D" + String(i + 1) + ")");
    uint8_t j_old = j;
    for (; j < sensorCount[i] + j_old; j++) {               // Alternativ: ; "for(uint8_t j = 0; j < sensorCount[i]; j++){...}" und [j + j_old] (statt [j]) in Elementaufruf
      message += "{\"ArrivalTime\": \"" + String(currentDate) + " " + String(timeClient.getFormattedTime()) + "\",\"TP\": \"TMP\",\"Tag\": \"" + String(j+1) + "\",\"Val\": \"" + String(ds18b20.at(j).temperature) + "\"}";
      if ((i + 1) == 4 && (j + 1) == (sensorCount[i] + j_old)) message += "]";
      else message += ", ";
    }
  }
  Serial.println("[SDwriteTemp()] Messwerte wurden in \"message\" gespeichert");
  Serial.println("[SDwriteTemp()] currentDate = " + currentDate);
  Serial.println("[SDwriteTemp()] currentTime = " + currentTime);
  Serial.println("[SDwriteTemp()] timeClient(hh.mm.ss) = timeClient.getHours() + timeClient.getMinutes() + timeClient.getSeconds() = " + String(timeClient.getHours()) + "." + String(timeClient.getMinutes()) + "." + String(timeClient.getSeconds()));
  String strFile = currentDate + "_" + String(timeClient.getHours()) + "." + String(timeClient.getMinutes()) + "." + String(timeClient.getSeconds()) + ".txt"; // timeClient.getFormattedTime() funktioniert hier nicht, weil es Doppelpunkte enthält, welche in Dateinamen nicht erlaubt sind. "currentTime" kann ebenfalls nicht verwendet werden, weil das Programm die Variable ganz umwandelt (siehe Konsole). Bei "currentDate" passiert dies komischerweise nicht
  File fp = SD.open(strFile.c_str(), FILE_WRITE);
  if (fp) {
    Serial.println("[SDwriteTemp()] Temperaturen werden gespeichert!");
    fp.println(message);
    fp.close();
    Serial.println(message);   // DEBUG
  } else {
    Serial.println("[SDwriteTemp()] Fehler beim Öffnen der Datei " + strFile);
  }
  //============================ Debug ====================================
  /*
    fp = SD.open(strFile);
    if (fp) {
    Serial.println("[SDwriteTemp/Debug] Öffne datalog.txt:");

    // read from the file until there's nothing else in it:
    while (fp.available()) {
      Serial.write(fp.read());
    }
    // close the file:
    fp.close();
    } else {
    // if the file didn't open, print an error:
    Serial.println("[SDwriteTemp/Debug] error opening datalog.txt");
    }
  */
  //========================================================================
  Serial.println("");
}

void NTPBegin(NTPClient &timeClient) {
  timeClient.begin();
  Serial.println("[NTPBegin() / SDCard] timeClient.begin()\n");
}

void NTPUpdate(NTPClient &timeClient) {
  timeClient.update();

  epochTime = timeClient.getEpochTime();        //NTP
  formattedTime = timeClient.getFormattedTime();
  currentHour = timeClient.getHours();
  currentMinute = timeClient.getMinutes();
  currentSecond = timeClient.getSeconds();
  weekDay = weekDays[timeClient.getDay()];
  ptm = gmtime ((time_t *)&epochTime);  //Get a time structure
  monthDay = ptm->tm_mday;
  currentMonth = ptm->tm_mon + 1;
  currentMonthName = months[currentMonth - 1];
  currentYear = ptm->tm_year + 1900;
  currentDate = String(currentYear) + "-" + String(currentMonth) + "-" + String(monthDay);  //Print complete date:
  currentTime = String(currentHour) + "." + String(currentMinute) + "." + String(currentSecond);

  Serial.println("[NTPUpdate()] timeClient.getFormattedTime() = " + timeClient.getFormattedTime()); // DEBUG
  //Serial.println(timeClient.getFormattedTime());
  Serial.println("[NTPUpdate()] currentDate = " + String(currentDate));
  Serial.println("[NTPUpdate()] currentTime = " + String(currentTime));
  Serial.println("");
}
